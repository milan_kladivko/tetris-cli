#
#  Here's the key bindings and the functions for their usage.
#
#  In the future, consider moving the keybinds in a user
#  config file for better usage.
#
#  In the distant future, add a keybind-changing screen
#  to some game menu.
#

import logging as log


keys_filedata = """


[Keybindings]


#  You can configure the keybindings in this section.
#  The lines are in this format: 
#   <key>SPACE<action>
#  
#  The keybindings don't do error checks, try to not mess things up.
#  Before changing, copy the lines and comment one of them by writing
#  a `#' before it, then modify the other duplicate line as you wish.
#   
#  
#  Possible user actions: 
#  
#    move_left    ... moves the current piece a tile left
#   
#    move_right   ... and right
#   
#    fall_faster  ... moves the piece down by a tile immediatelly
#                     after pressing the button
#  
#    slam         ... moves the piece down as far as it goes
#                     and immediatelly and places it down 
#  
#    spin_clck    ... spin the piece clockwise
#  
#    spin_cntr    ... spin the piece counter-clockwise
#    
#    pause        ... pauses the game
#    
#    force_quit   ... quits the game immediatelly
#    
#    force_reset  ... resets the game immediatelly
#  


I move_left
O move_right

E fall_faster
SPACE slam

H spin_clck
T spin_cntr

P pause
Q force_quit
R force_reset


"""


COMMENT_CHARACTER = "#"
SECTION_CHARACTER = "["
SEPARATOR = " "
INDEX_KEY = 0
INDEX_ACTION = 1


class Keybinds:

    key_to_action = dict()
    action_to_key = dict()

    def add(self, key, action):

        modified = (key in self.key_to_action.keys()) or (action in self.action_to_keys.keys())

        # two-way binding, just in case
        self.key_to_action[key] = action
        self.action_to_key[action] = key

        return modified

    def __str__():
        string = ""
        for key, action in self.key_to_action.items():
            line = "[%s] : %s\n" % (key, action)
            string += line
        return string


def keybinds_from_text(text):

    log.debug("[Load_Keybinds]")
    keybinds = Keybinds()

    for line in text.splitlines():
        line = line.strip()
        log.debug("Processing line:\n " + line)

        if is_commented(line):
            log.debug("commented.")
            pass

        else:

            components = line.split(SEPARATOR)

            if len(components) >= max(INDEX_KEY, INDEX_ACTION) + 1:

                key = components[INDEX_KEY]
                action = components[INDEX_ACTION]
                log.debug("key=%s \naction=%s" % (key, action))

                modified = keybinds.add(key, action)
                if modified:
                    log.debug("...modified")

            else:
                log.debug(";err; not enough components, skipping")
                pass

        log.debug("\n")

    return keybinds


def is_commented(line):
    return line.startswith(COMMENT_CHARACTER)


def load_keybinds_from_file(filename):
    raise Exception("not implemented yet")
    pass
