# File for testing

import logging as log
from tetris.__init__ import Piece
from tetris.utils import add_tuples


def enable_console_logging(logging_instance):
    """ Set the logger to add stdout (console) logging """

    import sys
    import logging

    stdout_logging = logging.StreamHandler(sys.stdout)
    logging_instance.getLogger().addHandler(stdout_logging)


# Reconfigure logger to also print to stdout

enable_console_logging(log)


#########################################################


def test__parse_data(piece_type):

    data = Piece.parse_data(piece_type)
    log.debug(data)


# test__parse_data('TRIANGLE')
# test__parse_data('LINE')

# -------------------------------------------------------


def test__add_tuple():
    a = (1, 2)
    b = (3, 4)

    c = add_tuples(a, b)
    print(c)
