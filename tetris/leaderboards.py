import os.path
import logging as log


LB_FILENAME = "./LEADERBOARDS.TXT"
SORT_BY = "score"
RECORD_ONLY_TOP = 100


HEADER_GROUPS = ("Name", "Lvl", "Lines", "Score")

HEADER = (
    """
 %5s | %-3s | %-7s | %-12s 
"""
    % HEADER_GROUPS
)

LINE_VALUE_SEP = ":"
LINE_FORMAT = " %5s : %-3s : %-7s : %-12s \n"


def leaderboard_file_exists(filename=LB_FILENAME):
    # consider checking header??
    # maybe check the format of the file too
    return os.path.isfile(filename)


def read_leaderboard_entries(filename=LB_FILENAME):

    # Read the file, filter the entries

    with open(filename, "r") as lb_file:
        all_lines = lb_file.readlines()
    entry_lines = all_lines[HEADER.count("\n") :]

    # Collect the entry data into a list of dicts

    entries = []
    for entry_line in entry_lines:

        entry_values = entry_line.split(LINE_VALUE_SEP)
        entry = {}

        for i in range(len(entry_values)):
            type = HEADER_GROUPS[i].lower()
            value = entry_values[i].strip()

            if type == "name":
                entry[type] = str(value)
            else:
                entry[type] = int(value)

        entries.append(entry)

    return entries


def format_to_line(entry):

    value_tuple = ()
    for i in range(len(HEADER_GROUPS)):
        type = HEADER_GROUPS[i].lower()
        value_tuple += (entry[type],)  # concat a 1-elem tuple

    return LINE_FORMAT % value_tuple


def write_new_leaderboard(entry_list, filename=LB_FILENAME):

    with open(filename, "w") as lb_file:

        lb_file.write(HEADER)

        for entry in entry_list:
            line = format_to_line(entry)
            lb_file.write(line)


# @todo; ADD THE CURRENT DATE


def construct_entry(stats, player_name):
    # @note; these names must be same as HEADER_GROUPS strings
    # (case insensitive)
    return {
        "name": player_name,
        "lvl": stats.level.current,
        "lines": stats.entries["line_cleared"].counter,
        "score": stats.calculate_score(),
    }


def record_player_results(game_data, player_name="anonym"):

    new_entry = construct_entry(game_data.stats, player_name)

    if not leaderboard_file_exists():
        entries = [new_entry]

    else:
        entries = read_leaderboard_entries()

        insert_at_index = 0
        for entry in entries:
            if entry[SORT_BY] <= new_entry[SORT_BY]:
                break
            else:
                insert_at_index += 1

        if insert_at_index < RECORD_ONLY_TOP or RECORD_ONLY_TOP < 0:
            entries.insert(insert_at_index, new_entry)

    write_new_leaderboard(entries)
