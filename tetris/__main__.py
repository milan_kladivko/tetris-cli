#
#  A console Tetris game using the Curses module.
#
#  Author: Milan Kladivko; Spring of 2019
#  Git: gitlab.com/milan_kladivko/tetris-curses
#
# ----------------------------------------------------------
#
#  The entry point to the program.
#  Here you'll find the curses configuration, other inits
#  and - of course - the main game loop.
#


# Debug and logging tools
# @incomplete; respond to a console argument for debug flag?
import logging as log
import curses
import time

# game-implementation-specific imports

# game data, logic and drawing
import tetris.resources as resources
import tetris.logic as logic
import tetris.drawing as draw
import tetris.game_timer as game_timer

# utility functions
from tetris.utils import *

# read-only configuration globals (i know what i'm doing)
from tetris.config import *

# only function I need from leaderboards
from tetris.leaderboards import record_player_results


log.basicConfig(filename="app.log", filemode="w", level=log.DEBUG, format="%(message)s")
log.debug("App start.")


# ============================================================
#  Curses writing utils
# ============================================================


def delay_for_secs(seconds):
    return time.sleep(seconds)


def key_block(window):
    """ Halt the whole program until key is pressed. """
    window.nodelay(0)


def key_permit_empty(window):
    """ Don't halt for new key; either get current or continue """
    window.nodelay(1)


def BOLD_ON(window):
    window.attron(curses.A_BOLD)


def bold_off(window):
    window.attroff(curses.A_BOLD)


def write_string_lines(string_arr, window, offset=(0, 0), line_sleep=0, char_sleep=0):
    """
    Write out a list of string lines to the Curses window
    with an offset tuple (x,y) and an optional delay
    after writing each line or character of the output.
    """

    assert type(string_arr) == list and len(string_arr) > 0
    assert type(offset) == tuple and len(offset) == 2
    offset_x = offset[1]
    offset_y = offset[0]

    line_number = 0
    for line in string_arr:

        if char_sleep <= 0:
            window.addstr(offset_y + line_number, offset_x, line)
        else:
            char_n = 0
            for char in line:
                window.addch(offset_y + line_number, offset_x + char_n, char)
                window.refresh()
                char_n += 1
                delay_for_secs(char_sleep)

        window.refresh()
        line_number += 1
        delay_for_secs(line_sleep)

    last_written_line = offset_y + line_number
    return last_written_line


def play_intro_credits(window):

    _w = window  # window shorthand
    _w.erase()

    # Make a window border with a title
    _w.border()
    _w.addstr(0, 3, " INTRO CREDITS MESSAGE ")
    _w.refresh()

    LEFT_MARGIN = 3

    delay_for_secs(0.5)
    BOLD_ON(_w)
    block_end = write_string_lines(["Tetris-CLI ", "`````````` "], _w, (2, LEFT_MARGIN))

    delay_for_secs(0.5)
    bold_off(_w)
    block_end = write_string_lines(
        [
            "A console Tetris game made in Python ",
            "with the help of the Curses module. ",
            "",
            "Despite being made for personal educational",
            "purposes, I've tried to make not only",
            "a functional but also an enjoyable and",
            "playable tetris implementation.",
            "I hope you'll like it!",
        ],
        _w,
        (block_end, LEFT_MARGIN),
        0,
        0.01,
    )

    BOLD_ON(_w)
    block_end = write_string_lines(
        ["Made By: Milan Kladivko"], _w, (block_end + 1, LEFT_MARGIN + 10), 0, 0.04
    )

    delay_for_secs(0.5)
    block_end = write_string_lines(
        [
            "Press [C] to [C]ontinue...",
            "[D]on't show this intro again. {not done}",
            "[Q]uit the game.",
        ],
        _w,
        (block_end + 2, LEFT_MARGIN),
        0.2,
        0,
    )
    bold_off(_w)

    # Handle user interaction
    key_block(_w)
    while True:
        key = _w.getch()
        if key == ord("c"):
            log.debug("Advanced from intro to the menu")
            _w.erase()
            _w.border()
            BOLD_ON(_w)
            write_string_lines(["Have fun!"], _w, (block_end - 3, LEFT_MARGIN))
            bold_off(_w)
            _w.refresh()
            delay_for_secs(1)
            break
        elif key == ord("d"):
            # edit config
            log.debug("Tried to turn off intro message")
        elif key == ord("q"):
            log.debug("Quitting from intro screen")
            exit_app(window)


def pause_game(window):
    """
    Pauses curses, displays a message and asks the user
    for what to do next.
    """
    W = window  # just a shorthand

    # Pause the game, prompt the player
    W.erase()
    W.border()

    # Pause notice
    BOLD_ON(W)
    W.addstr(0, 4, " GAME PAUSED ")
    bold_off(W)

    # What to while paused
    block_end = write_string_lines(["  [Q]uit", "Un[P]ause", " ",], W, (3, 2))

    # List game controls
    BOLD_ON(W)
    W.addstr(block_end + 1, 2, "Game Controls:")
    bold_off(W)
    block_end = write_string_lines(
        [
            "",
            " [a]   [s]   [d]",
            " Left  Fall  Right",
            "",
            "       [j] [k]",
            " Rotate +   -",
            "",
            " Slam! [space]",
            "",
            " Pause [p] or [esc]",
            "",
            "",
        ],
        W,
        (block_end + 2, 2),
    )

    W.refresh()

    # Get the player's response key
    key_block(W)
    key = W.getch()

    if key == ord("q"):
        exit_app(window)

    else:
        # If we don't get the quit key, restore window state
        key_permit_empty(W)
        return


def user_wants_to_record(player_name):
    return player_name != "DONT_RECORD"


def ask_user_for_name(w):

    _end = write_string_lines(
        [" Your name?:        ", "                    ", "      [ _____ ]     ",], w, (2, 1)
    )
    w.refresh()

    hardcoded_name_pos = (_end - 1, 9)  # start of name spots
    write_string_lines([""], w, hardcoded_name_pos)  # jump cursor there

    NAME_CHARS = " abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
    NAME_LEN = 5
    written_chars = 0
    player_name = ""

    key_block(w)
    BOLD_ON(w)
    while written_chars < NAME_LEN:
        char = chr(w.getch())  # get the character from code
        if char in NAME_CHARS:
            w.addch(char)
            w.refresh()
            written_chars += 1
            player_name += char
    bold_off(w)

    delay_for_secs(0.3)

    # delete bracket input indicators
    write_string_lines(["  "], w, (_end - 1, 7))
    write_string_lines(["  "], w, (_end - 1, 14))

    delay_for_secs(0.6)

    write_string_lines(
        [" [w] Record score ", " [r] Rename ", " [n] Don't record "], w, (_end + 1, 1)
    )

    key_block(w)
    choice_record = w.getch()
    while choice_record not in [ord("w"), ord("r"), ord("n")]:
        choice_record = w.getch()

    if choice_record == ord("w"):
        # RECORD TO LEADERBOARDS
        write_string_lines(
            [" Your score will ", " be added to ", " LEADERBOARDS.TXT "], w, (_end + 1, 1), 0, 0.01
        )

    elif choice_record == ord("n"):
        # DON'T RECORD ANYTHING
        write_string_lines(
            ["                   " " Your score won't  ", " be recorded. ",],
            w,
            (_end + 1, 1),
            0.03,
        )
        player_name = "DONT_RECORD"

    elif choice_record == ord("r"):
        # WIPE AND RECURSIVELY ASK AGAIN
        gameover_screen_wipe(w, 0.0)
        return ask_user_for_name(w)

    # end the dialog
    delay_for_secs(0.5)
    key_permit_empty(w)
    return player_name


def exit_app(window):
    w = window

    w.erase()
    w.border()
    w.addstr(1, 1, " Exiting game... ")
    w.refresh()

    delay_for_secs(0.8)
    w.addstr(3, 11, " Bye!! ")
    w.refresh()

    delay_for_secs(0.5)
    exit(1)


def gameover_screen_wipe(w, speed=0.02):

    BOLD_ON(w)
    write_string_lines([" Game Over "], w, (0, 2))
    screen_wipe_lines = []
    for i in range(HEI * HEI_SCALE):
        screen_wipe_lines.append("                    ")
    write_string_lines(screen_wipe_lines, w, (1, 1), speed)
    bold_off(w)

    delay_for_secs(0.5)


def gameover_ask_if_restart(w):

    BOLD_ON(w)
    write_string_lines([" [R] Start again? "], w, (10, 1), 0, 0.01)
    bold_off(w)

    key_block(w)
    while True:  # Ignore keys invalid for this menu
        game_over_screen_response = w.getch()
        if game_over_screen_response == ord("r"):
            restart = True
            break
        elif game_over_screen_response == ord("q"):
            restart = False
            break

    key_permit_empty(w)

    return restart


def init_curses_colors():
    # @todo; move to `resources.py'??
    if not USE_COLOR:
        return

    # color shorthands
    white = curses.COLOR_WHITE
    black = curses.COLOR_BLACK
    red = curses.COLOR_RED
    blue = curses.COLOR_BLUE
    green = curses.COLOR_GREEN
    cyan = curses.COLOR_CYAN
    magenta = curses.COLOR_MAGENTA
    yellow = curses.COLOR_YELLOW

    # an initial color to reset to
    curses.init_pair(1, white, black)

    # game tile colors
    for tile_type, color_pair in {
        "EMPTY": [white, black],
        "FULL": [black, white],
        "FLY_TILE": [black, red],
        "FLY_HANDLE": [black, red],
        "HIGHLIGHT": [black, red],
    }.items():
        curses.init_pair(resources.Tiles[tile_type].color, color_pair[0], color_pair[1])

    for piece_name, curses_color in {
        "I": red,
        "O": blue,
        "T": yellow,
        "S": green,
        "Z": cyan,
        "J": white,
        "L": magenta,
    }.items():
        curses.init_pair(resources.Tiles[piece_name].color, black, curses_color)


def window_placement_test(term, wg, wb):

    term.getch()

    wb.bkgd(".")
    wb.border()
    wb.refresh()

    term.getch()

    wg.bkgd(".")
    wb.border()
    wg.refresh()

    term.getch()

    wg.bkgd(" ")
    wb.bkgd(" ")


def run(term):

    """ The main function of the program """

    init_curses_colors()

    #
    # Create the game windows
    #
    sidebar_window = curses.newwin(
        HEI * HEI_SCALE + 2 * BORDER, SIDEBAR_WID * WID_SCALE + 2 * BORDER
    )
    # Move the sidebar next to the maingame-window
    sidebar_window.mvwin(0, WID * WID_SCALE + 2 * BORDER)

    game_window = curses.newwin(HEI * HEI_SCALE + 2 * BORDER, WID * WID_SCALE + 2 * BORDER)

    # Test the window placement
    # window_placement_test(term, game_window, sidebar_window)

    #
    # Game data initialization
    #
    data = logic.init_data(WID, HEI)

    #
    # Start the main menu
    #
    MAIN_MENU_LINES = 20
    MAIN_MENU_WIDTH = 50
    main_menu_window = curses.newwin(MAIN_MENU_LINES, MAIN_MENU_WIDTH)
    if not SKIP_INTRO_CREDITS:
        play_intro_credits(main_menu_window)

    # Erase remains of the main window when exited out of it
    term.erase()
    term.refresh()

    #
    # Timer for time deltas and time-based events
    #
    timer = game_timer.Game_Timer()
    timer.init_start_tick()
    timer.add_interval("fly_descent", 0.5)
    timer.add_interval("seconds", 1)
    timer.add_interval("minutes", 60)
    timer.add_interval("deletion_delay", LINE_DELETION_DELAY_SECS)
    timer.intervals["deletion_delay"].running = False

    #
    # Game loop
    #
    key_permit_empty(game_window)  # don't wait for input to game_window
    data.redraw_needed = True  # start with a redraw
    while True:

        # Using framerate computation compensation
        time_before_computing = time.perf_counter()

        # Put a gap in log between frames
        log.debug("===================================== frame ===")

        # Check GAME OVER
        if data.game_ended:

            gameover_screen_wipe(game_window)

            player_name = ask_user_for_name(game_window)
            if user_wants_to_record(player_name):
                record_player_results(data, player_name)

            restart = gameover_ask_if_restart(game_window)

            if restart:
                data = logic.init_data(WID, HEI)
                timer.reset_all_intervals()
                timer.init_start_tick()
            else:
                exit_app(game_window)

        # Keyboard controls handling; handle user input
        user_input_key = game_window.getch()

        # Check for pausing
        if user_input_key == ord("p") or user_input_key == ESCAPE_KEY:

            # handle paused game, continue on if unpaused
            pause_game(game_window)
            # then after unpausing...

            # get a new gameplay user key for this frame
            user_input_key = game_window.getch()

            # don't count time
            timer.record_tick(False)
            # ignore triggers occured while paused, reset elapsed to 0
            timer.reset_all_intervals()  # @BUG; if counting time, we'll have some problems with resetting all the intervals
            # redraw plane immediatelly after unpausing
            data.redraw_needed = True

        # Change game state based on time and user input
        timer.record_tick()
        logic.step_data(data, timer, user_input_key)

        # Draw game data to windows, if data changed visibly
        if data.redraw_needed:

            # Wipe the backgrounds
            game_window.erase()
            sidebar_window.erase()

            # Draw the content
            draw.draw_game_plane(data, timer, game_window)
            draw.draw_sidebar(data, timer, sidebar_window)

            # Make borders around the windows
            if BORDER > 0:
                draw.reset_color_to_default(game_window)
                game_window.border()
                sidebar_window.border()

            # Draw the windows' changes onto terminal screen
            game_window.refresh()
            sidebar_window.refresh()

        # Reset the flag for redrawing
        data.redraw_needed = False

        # Wait between "frames", compensate time taken by computing
        DEBUG_FRAMERATE_PERFORMANCE = False
        time_computing_done = time.perf_counter()
        time_already_spent = time_computing_done - time_before_computing
        time_remaining_till_frame = FRAME_LEN - time_already_spent
        lagging = time_remaining_till_frame < 0
        if not lagging:
            delay_for_secs(time_remaining_till_frame)

            if DEBUG_FRAMERATE_PERFORMANCE:
                perf_perc = round(time_already_spent / FRAME_LEN * 100)
                log.debug("Computing took {}% of frame".format(perf_perc))

        else:
            delay_for_secs(0.001)
            log.warn("[PERF_WARN] Frame lagged.")


# Run curses through the wrapper
# so I don't have to bother with it
if __name__ == "__main__":
    curses.wrapper(run)
