#
#  Here's the tetris-game-specific logic of the program.
#  This includes how the piece moves on user's input,
#  how the data behaves and changes etc.
#
#  There shouldn't be anything graphical except for
#  changing a flag telling the drawing module that a draw
#  would make the screen look different.
#


import logging as log
import random
from enum import Enum

from tetris.resources import Pieces, Tiles
from tetris.config import X_INDEX, Y_INDEX

from tetris.utils import filled_2d_arr, Deny_New_Fields, setyx, plane_str
from tetris.utils import add_tuples, add_all_tuples, getyx, sign


# ===========================================================
#  Piece data struct
# ===========================================================


class Piece:
    def __init__(self, type):

        self.type = type

        self.data = Pieces[type]

        self.rotation = 0  # 0123
        self.x = 0
        self.y = 0
        self.color = None  # @not_implemented; use distinct colors
        # @idea; don't use color, we can extract from type

    def __str__(self):
        return "{}[y{},x{},rot{}]".format(self.type, self.y, self.x, self.rotation)

    def make_copy(self):
        piece = make_new_piece(self.type)
        piece.rotation = self.rotation
        piece.x = self.x
        piece.y = self.y
        return piece


def make_new_piece(type):
    return Piece(type)


# ===========================================================
#  Gameplay statistics (information only)
# ===========================================================


class Stats:
    def __init__(self):
        # @not_implemented; store scoreable stats with the level they happened at so we can have interesting scores /// probably use an array where the index is level and values are number of occurences of stat
        # @idea; have a speedrun score where we use time_seconds with the stats ...maybe even combo slams etc
        self.time_seconds = 0

        # level, including the level-up mechanics and dependencies
        self.level = Level()

        # stats that make up the score
        self.entries = {
            "piece_planted": Planted_Stat(),
            "piece_slammed": Slammed_Stat(),
            "line_cleared": LineCleared_Stat(),
        }

    def calculate_score(self):
        total_score = 0
        for stat in self.entries.values():
            total_score += stat.summed_score
        return total_score

    def add_to(self, entry_name, n=1):
        assert entry_name in self.entries
        # Record the lines cleared for level-checking contexts
        if entry_name == "line_cleared":
            self.level.account_lines_cleared(n)

        stat = self.entries[entry_name]
        stat.counter += n
        stat.summed_score += stat.score_of(self.level.current, n)


class Level:
    def __init__(self):
        self.current = 0
        self.target_lines_vargoal = 0
        self.value_lines_vargoal = 0
        self.combo_values = [1, 3, 5, 8]

        self.gained_this_frame = False  # modified from the outside directly

    def account_lines_cleared(self, n):
        self.value_lines_vargoal += self.combo_values[n - 1]
        if self.value_lines_vargoal > self.target_lines_vargoal:
            self.next_level()

    def next_level(self):
        self.current += 1
        self.value_lines_vargoal = 0
        self.target_lines_vargoal = self.current * 5


class Stat:
    def __init__(self):
        self.counter = 0
        self.summed_score = 0

    def score_of(self, level, n):
        raise Exception("Use inheritance")


class Planted_Stat(Stat):
    def score_of(self, level, n):
        val = n * 5
        return val


class Slammed_Stat(Stat):
    def score_of(self, level, n):
        val = n * 3
        return (val) * (level + 1)


class LineCleared_Stat(Stat):
    def score_of(self, level, lines_at_once):
        values = [5, 7, 10, 15]
        val = values[lines_at_once - 1]
        return (val) * (level + 1)


# ===========================================================
#  Game data
# ===========================================================


class Game_Data(Deny_New_Fields):
    def __init__(self, width, height):

        self.plane = filled_2d_arr(width, height, Tiles["EMPTY"])
        self.width = width
        self.height = height

        # whether a new draw would look different from previous draw
        self.redraw_needed = False

        # lines to get deleted
        # ...not destroyed immediately so we can highlight them
        #    and/or put a delay on deletion
        self.lines_to_delete = None

        self.stats = Stats()
        self.game_ended = False

        # piece being controlled by user
        self.flying = None
        self.flying_ghost = None  # a moved copy of .flying
        # piece to be added when .flying is placed down
        self.upcoming = None

        self.block_input = False

        self._deny_new_fields()  # Necessary for denying fields


def init_data(plane_width, plane_height):
    return Game_Data(plane_width, plane_height)


def end_the_game(game_data):
    log.debug("\n\n  [ GAME_OVER ]")
    game_data.game_ended = True


def plant_flying(game_data, timer):

    log.debug("\n[PLANTING_PIECE] :: {}".format(game_data.flying))

    fly = game_data.flying
    coords = [fly.y, fly.x]
    positions = add_all_tuples(fly.data[fly.rotation], coords)
    positions.append(coords)

    plane = game_data.plane
    modified_lines = []
    for tile in positions:
        planted_tile_above_window = tile[Y_INDEX] < 0
        if planted_tile_above_window:
            end_the_game(game_data)
            return
        modified_lines.append(tile[Y_INDEX])
        setyx(plane, tile[Y_INDEX], tile[X_INDEX], Tiles["FULL"])

    game_data.flying = None  # delete flying, fill later from upcoming
    timer.intervals["fly_descent"].full_reset()  # sync the tile descent
    game_data.stats.add_to("piece_planted")

    # @warning; this bit is important!!
    mark_full_lines_for_deletion(game_data, timer, modified_lines)


def collapse_line(line, game_data):

    plane = game_data.plane
    y_to = line
    y_from = line - 1  # above the 'to' line

    log.debug("  destroying line {} ::".format(line))
    log.debug("...before...")
    log.debug(plane_str(plane))

    while True:
        line_from_was_empty = True

        for x in range(game_data.width):

            tile_from = getyx(plane, y_from, x)
            setyx(plane, y_to, x, tile_from)

            if tile_from is Tiles["FULL"]:
                # @warning; finish moving this line! break cannot be here
                line_from_was_empty = False

        if line_from_was_empty:
            break  # @warning; break after we moved the empty tiles
        else:
            # next time, collapse the line above
            y_to -= 1
            y_from -= 1

    log.debug("...after...")
    log.debug(plane_str(plane))


def mark_full_lines_for_deletion(game_data, timer, modified_lines):

    full_lines = set()  # for duplication avoidance
    for y in modified_lines:

        line_is_full = True
        for x in range(game_data.width):
            if getyx(game_data.plane, y, x) is not Tiles["FULL"]:
                line_is_full = False

        if line_is_full:
            full_lines.add(y)  # try to add to the set

    if len(full_lines) == 0:
        game_data.lines_to_delete = None
    else:
        game_data.lines_to_delete = full_lines
        log.debug("[MARK_DESTROY] {}".format(full_lines))

        del_delay = timer.intervals["deletion_delay"]
        del_delay.full_reset()
        del_delay.running = True

        game_data.redraw_needed = True


def delete_marked_lines(game_data):

    marked_lines = game_data.lines_to_delete
    if marked_lines is not None:

        log.debug("\n[DESTROY_MARKED_LINES]")
        # @warning; be sure to go top-to-bottom (lines change)
        for line in sorted(marked_lines):
            collapse_line(line, game_data)
        game_data.lines_to_delete = None
        game_data.stats.add_to("line_cleared", +len(marked_lines))

        game_data.redraw_needed = True
        game_data.flying_ghost = ghost_of(game_data.flying, game_data)


upcoming_piece_bag = list()


def generate_random_piece(y, x):
    # choose the piece from a shuffled bag
    global upcoming_piece_bag
    if len(upcoming_piece_bag) == 0:
        upcoming_piece_bag = list(Pieces.keys())
        random.shuffle(upcoming_piece_bag)
    piece_type = upcoming_piece_bag.pop()

    piece = make_new_piece(piece_type)
    piece.x = x
    piece.y = y
    return piece


def handle_descent(game_data, timer):

    if game_data.flying is None:
        log.debug("[HANDLE_DESCENT] no fly!")
        return

    fly_descent_event = timer.intervals["fly_descent"]
    times_piece_should_descend = fly_descent_event.triggered

    if times_piece_should_descend != 0 and timer.intervals["deletion_delay"].running == False:

        log.debug("[FLY_DESCEND] {}".format(times_piece_should_descend))

        fly_descent_event.reset_trigger()
        game_data.redraw_needed = True

        for i in range(times_piece_should_descend):
            collided_when_falling = fly_fall(game_data, timer)
            if collided_when_falling:
                plant_flying(game_data, timer)


def ghost_of(piece, game):
    ghost = piece.make_copy()
    log.debug("[GHOST_UPDATE] %s" % ghost)
    while True:
        cannot_fall_more = move(ghost, [+1, 0], game)
        # @bug; apparently we don't have the piece data????
        if cannot_fall_more:
            break
    return ghost


def pull_new_piece_to_game(game_data) -> None:
    start_y = -1
    start_x = 4  # somewhere near the center

    log.debug("[PULL_NEW_PIECE]")

    # @incomplete; the upcoming check is clunky??
    if game_data.upcoming is None:
        game_data.upcoming = generate_random_piece(start_y, start_x)

    game_data.flying = game_data.upcoming
    game_data.upcoming = generate_random_piece(start_y, start_x)

    game_data.redraw_needed = True


def step_data(game_data, timer, user_input_key):
    """
    Make changes to game state.
    Uses the timer to keep deltas and time-based interval triggers;
    uses the user input to make changes to also make changes
    to the timer triggers (e.g. pause after slamming; resetting the
    falling of the piece after placement).
    """

    # Record elapsed game time
    game_data.stats.time_seconds += timer.prev_tick

    del_delay = timer.intervals["deletion_delay"]

    # Get a new flying piece if previous was planted
    need_new_fly = game_data.flying is None
    if need_new_fly:
        pull_new_piece_to_game(game_data)
        game_data.flying_ghost = ghost_of(game_data.flying, game_data)

        # don't release input block when deletion_delay is on
        if del_delay.running is False:
            game_data.block_input = False  # reset input blocking (slams)

    # Handle input, then descend the flying piece
    handle_input_key(user_input_key, game_data, timer)
    handle_descent(game_data, timer)

    # Prepare for level check
    level = game_data.stats.level
    prev_level = level.current

    # Delete lines marked for deletion after a delay
    if del_delay.triggered:
        del_delay.reset_trigger()
        del_delay.running = False

        delete_marked_lines(game_data)
        game_data.block_input = False

    # Mark changed level
    # this can't be in next_level(), I can't reset the flag from inside
    level.gained_this_frame = prev_level != level.current

    # Update the falling speed based on current level
    speed = descent_speed_of_level(game_data.stats.level.current)
    timer.intervals["fly_descent"].target = speed


def descent_speed_of_level(level):
    speed = [
        1.00000,
        0.79300,
        0.61780,
        0.47273,
        0.35520,
        0.26200,
        0.18968,
        0.13473,
        0.09388,
        0.06415,
        0.04298,
        0.02822,
    ]
    return speed[level]


# ===========================================================
#  Collision detection
# ===========================================================


def coords_are_outside_plane(coords, plane):
    height = len(plane)
    width = len(plane[0])
    x = coords[X_INDEX]
    y = coords[Y_INDEX]
    x_failed = x < 0 or x >= width
    y_failed = y >= height
    return x_failed or y_failed


def piece_would_collide(game_data, piece, offset, rotation=None):

    # Get the relative position of the tiles
    # @incomplete; come back to this when caching of tile positions is done - we'll get a slight performance increase
    if rotation is None:
        piece_tiles = piece.data[piece.rotation]
    else:
        piece_tiles = piece.data[rotation]
    position = add_tuples([piece.y, piece.x], offset)
    tiles_at_pos = add_all_tuples(piece_tiles, position)
    tiles_at_pos.append(position)  # add the anchor to tiles

    # test the tiles against the game plane
    for tile in tiles_at_pos:

        if coords_are_outside_plane(tile, game_data.plane):
            log.debug("\n[COLLISION] bounds @ {}".format(tile))
            return True

        if tile[Y_INDEX] >= 0:
            plane_tile = getyx(game_data.plane, tile[Y_INDEX], tile[X_INDEX])
            if plane_tile is Tiles["FULL"]:
                log.debug("[COLLISION] tile @ {}".format(tile))
                return True

    return False


# ===========================================================
#  Piece's move set (movement and rotation)
# ===========================================================


def move(piece, offset, game_data):
    if piece_would_collide(game_data, piece, offset):
        collided = True
    else:
        piece.x += offset[X_INDEX]
        piece.y += offset[Y_INDEX]
        collided = False
    return collided


def fly_go_right(game):
    collided = move(game.flying, [0, +1], game)
    # @warning; make ghost after moving
    game.flying_ghost = ghost_of(game.flying, game)
    return collided


def fly_go_left(game):
    collided = move(game.flying, [0, -1], game)
    # @warning; make ghost after moving
    game.flying_ghost = ghost_of(game.flying, game)
    return collided


def fly_fall(game, timer):
    collided = move(game.flying, [+1, 0], game)
    return collided


def fly_slam(game, timer):
    fell_n_lines = 0
    while True:
        collided = move(game.flying, [+1, 0], game)
        if collided:
            break
        else:
            fell_n_lines += 1
    if fell_n_lines > 1:
        game.stats.add_to("piece_slammed")

    # plant immediatelly, don't wait for next frame
    plant_flying(game, timer)

    # reset (sync) the next fall so it plants after the same time
    timer.intervals["fly_descent"].full_reset()
    # block input until we pull out a new fly piece
    game.block_input = True


def fly_rotate(game, direction):
    fly = game.flying
    piece_rotations = len(fly.data)
    rotation = (fly.rotation + sign(direction)) % piece_rotations
    offset = [0, 0]
    rot_collides = piece_would_collide(game, fly, offset, rotation)
    if rot_collides:
        # try if nudging can allow this rotation
        if not piece_would_collide(game, fly, [0, +1], rotation):
            fly.x += 1
            fly.rotation = rotation
        elif not piece_would_collide(game, fly, [0, -1], rotation):
            fly.x -= 1
            fly.rotation = rotation
        elif fly.type == "LINE":
            if not piece_would_collide(game, fly, [0, +2], rotation):
                fly.x += 2
                fly.rotation = rotation
            elif not piece_would_collide(game, fly, [0, -2], rotation):
                fly.x -= 2
                fly.rotation = rotation
        log.debug("[COLLIDED] Can't fit rotate")
    else:
        fly.rotation = rotation

    game.flying_ghost = ghost_of(game.flying, game)


# ===========================================================
#  Key Execution, Keybindings and Possible Player Actions
# ===========================================================


class User_Actions(Enum):

    # @incomplete; handle the binding of actions to keys and functions better - we need some mutable data variable, not this garbage

    IDLE = -1  # key code of no input from curses

    RIGHT = ord("d")
    LEFT = ord("a")
    FORCE_FALL = ord("s")
    SLAM = ord("h")  # ord(' ')

    ROTATE_PLUS = ord("k")
    ROTATE_MINUS = ord("j")


def read_keybinds_from_file(bindings, filename):
    # @not_implemented; consider after we've made sure the bindings are not implemented in a stupid counter-productive hacky way
    pass


def resolve_key_to_action(key):
    for action in User_Actions:
        if key == action.value:
            return action
    return User_Actions.IDLE


def execute_action(action, game_data, timer):

    # @incomplete; refactor this into some collection and automate function execution maybe? ...not needed but might be more readable

    game_data.redraw_needed = True  # @incomplete; @not_implemented; move this to every function - we won't have to redraw everything if we bump into the wall or something and nothing happens anyway.

    if action is User_Actions.RIGHT:
        fly_go_right(game_data)
    if action is User_Actions.LEFT:
        fly_go_left(game_data)

    if action is User_Actions.FORCE_FALL:
        fly_fall(game_data, timer)
    if action is User_Actions.SLAM:
        fly_slam(game_data, timer)

    if action is User_Actions.ROTATE_PLUS:
        fly_rotate(game_data, +1)
    if action is User_Actions.ROTATE_MINUS:
        fly_rotate(game_data, -1)


def handle_input_key(key, game_data, timer):
    assert isinstance(key, int)

    user_action = resolve_key_to_action(key)

    if user_action is not User_Actions.IDLE:
        # @Idea; record keypresses for replay maybe??
        log.debug("\n==[ {} ]==".format(user_action.name))

        if game_data.block_input is False:
            execute_action(user_action, game_data, timer)

        else:
            log.debug("   ...this action got blocked!!")
