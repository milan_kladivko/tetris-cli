#
#  Here is all the mechanics of acquiring game time.
#  This could be (@idea; and maybe should?) in the logic
#  module but putting it here seemed like a good idea
#  at the time.
#

# @idea; keep a frame_time history to use for getting an average framerate or something... probably not needed though


import logging as log
import time


class Game_Timer:
    def __init__(self):
        # remember the previous tick's time
        self.prev_tick = -1
        # various intervals
        self.intervals = dict()

    def init_start_tick(self):
        update_intervals = False
        self.record_tick(update_intervals)

    def record_tick(self, update_intervals=True):

        now = time.perf_counter()
        secs_between_ticks = now - self.prev_tick
        self.prev_tick = now

        if update_intervals:
            self.update_all_intervals_by(secs_between_ticks)

        return secs_between_ticks

    def add_interval(self, name_string, target, delay=0):
        is_name_new = not (name_string in self.intervals.keys())
        interval = None
        if is_name_new:
            interval = Interval(target, delay)
            self.intervals[name_string] = interval
        else:
            interval = self.intervals[name_string]
        return interval

    def update_all_intervals_by(self, elapsed):
        for int_name, interval in self.intervals.items():
            interval.add_time(elapsed)

            """
      if interval.triggered:
        log.debug('[trigger] {} ({}) '.format(
          int_name, interval.triggered))
      """

    def reset_all_intervals(self):
        for interval in self.intervals.values():
            interval.full_reset()

    def is_any_interval_forcing_redraw(self):
        for interval in self.intervals.values():
            if interval.force_redraws and triggered < 0:
                return True
        return False


class Interval:
    def __init__(self, target, initial_delay=0):
        assert target > 0

        self.target = target
        self.elapsed = -initial_delay
        self.triggered = 0
        self.running = True
        self.force_redraws = False

        self.initial_delay = initial_delay  # remember for full_reset()

    def reset_trigger(self):
        self.triggered = 0

    def full_reset(self):
        self.triggered = 0
        self.elapsed = -(self.initial_delay)

    def add_time(self, elapsed, leave_triggers=True):
        if self.running == False:
            return

        self.elapsed += elapsed

        if not leave_triggers:
            self.reset_trigger()

        if self.elapsed > self.target:

            triggered_while_elapsed = int(self.elapsed / self.target)
            self.triggered += triggered_while_elapsed

            self.elapsed %= self.target
