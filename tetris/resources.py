#
#  Contains human-readable data and the methods to format
#  it into program-usable data.
#


import logging as log
from tetris.config import Y_INDEX, X_INDEX
from tetris.utils import subtract_all_tuples


# ===========================================================
#  Types of tiles in the game
# ===========================================================


class Tile:
    START_COLOR_ID = 2
    instance_id = START_COLOR_ID

    def __init__(self, char, name=" "):
        self.char = char
        self.color = Tile.instance_id
        self.name = name
        Tile.instance_id += 1

    def __str__(self):
        return self.char


Tiles = {
    "EMPTY": Tile(" "),
    "FULL": Tile("O"),
    "FLY_TILE": Tile("X"),  # "█"),
    "FLY_HANDLE": Tile("%"),
    "HIGHLIGHT": Tile("~"),
}


def create_piece_type(char, name):
    new_type = Tile(char, name)  # create the type
    Tiles[name] = new_type  # add it to the tiles
    return new_type  # return it just in case


for piece_name in ["I", "O", "T", "S", "Z", "J", "L"]:
    create_piece_type(Tiles["FULL"].char, piece_name)


# @incomplete; consider using some kind of lookuppable ID so we don't have to store 200 instances of Tile['EMPTY'] in the Game_Data.plane


# ===========================================================
#  Data-string into a Piece's Rotation-data
#  array conversion
# ===========================================================


def rotation_tiledata_for_piece(type, piece_type_dict):
    """
    Get the tile data array for the given piece type.
    Returns an array of coordinates of tiles around the anchor.
    The anchor is always at coords [0,0] so it is omitted.
    """
    assert isinstance(type, str)
    assert isinstance(piece_type_dict, dict)

    piece_data_str = piece_type_dict[type]
    assert piece_data_str is not None, "Undefined piece: " + type

    log.debug("[LOAD_PIECE] " + type)

    data = []  # tile's rotation as index 0123
    rot_tiles = []
    anchor = None
    coords = [-1, -1]

    for line in piece_data_str.splitlines():

        line_is_rotation_separator = len(line.strip()) == 0
        data_was_obtained = len(rot_tiles) > 0

        if line_is_rotation_separator:

            coords[Y_INDEX] = 0

            if data_was_obtained:
                if anchor is None:
                    err = "[DATA_ERR] No anchor in {} rotation {}".format(type, len(data))
                    log.error(err)
                    raise Exception(err)

                # Shift all tuples so anchor is at [0, 0]
                anchored_rot_tiles = subtract_all_tuples(rot_tiles, anchor)
                data.append(anchored_rot_tiles)
                # Reset / flush temp values
                rot_tiles = []
                anchor = None

            log.debug(" -   -   -   -   -   -   ")

        else:

            log.debug("{}".format(line))

            coords[X_INDEX] = 0
            for char in line:
                if char == CHAR_ANCHOR:
                    anchor = coords.copy()
                elif char == CHAR_TILE:
                    rot_tiles.append(coords.copy())
                elif char == CHAR_PAD:
                    pass  # only increment coords
                else:
                    assert True, "Illegal char in tile data: '{}'".format(char)
                coords[X_INDEX] += 1

            coords[Y_INDEX] += 1

    # Finally return the parsed array of rotations' data
    return data


def full_tiledata_dict_for(dict_piece_types):
    data_dict = {}
    for type in dict_piece_types.keys():
        data = rotation_tiledata_for_piece(type, dict_piece_types)
        data_dict[type] = data
    return data_dict


# ===========================================================
#  Piece data strings; requires conversion above
# ===========================================================

CHAR_PAD = " "
CHAR_ANCHOR = "O"
CHAR_TILE = "X"
Piece_Types = {
    # Please spin clockwise with indexes or it'll feel weird
    "O": """
   OX
   XX
  """,
    "I": """
   XOXX

   X
   O
   X
   X
  """,
    "L": """
   X
   O
   XX

   XOX
   X

   XX
    O
    X

     X
   XOX
  """,
    "J": """
    X
    O
   XX

   X
   XOX

   XX
   O
   X

   XOX
     X
  """,
    "T": """
    X
   XOX

   X
   OX
   X

   XOX
    X

    X
   XO
    X
  """,
    "Z": """
   XO
    XX

    X
   XO
   X
  """,
    "S": """
    OX
   XX

   X
   OX
    X
  """,
}


# ===========================================================
#  Tiledata of all pieces reparsed for use in the app
#  i.e. what should actually be used
# ===========================================================

Pieces = full_tiledata_dict_for(Piece_Types)
