#
#  Here are the utility functions that are used throughout
#  the project.
#  
#  Think of these as a bare-minimum library of things that
#  I needed at that moment and putting it next to the code
#  didn't make much sense.




#===========================================================
#  Maths 
#===========================================================

def sign(number):
  if number > 0:
    return 1
  if number < 0:
    return -1
  return 0


#===========================================================
#  Tuple operations
#===========================================================

def add_tuples(a, b):
  assert type(a) is list or tuple
  assert type(b) is list or tuple
  
  n = min(len(a), len(b))
  ans = []
  for i in range(n):
    ans.append(a[i] + b[i])
  return ans


def subtract_tuples(a, b):
  assert type(a) is list or tuple
  assert type(b) is list or tuple
  
  ans = []
  n = min(len(a), len(b))
  for i in range(n):
    ans.append(a[i] - b[i])
  return ans

def subtract_all_tuples(array, the_one):
  assert type(array) is list
  assert type(array[0]) is list or tuple
  assert type(the_one) is list or tuple
  
  ans_array = []
  for tup in array:
    ans_array.append(subtract_tuples(tup, the_one))
  return ans_array

def add_all_tuples(array, the_one):
  assert type(array) is list
  assert type(array[0]) is list or tuple
  assert type(the_one) is list or tuple
  
  ans_array = []
  for tup in array:
    ans_array.append(add_tuples(tup, the_one))
  return ans_array


#===========================================================
#  2D array manipulation 
#===========================================================

def filled_2d_arr(width, height, fill_element):

  array = []

  for y in range(height):
    line = []
    for x in range(width):
      line.append(fill_element)
      
    array.append(line)

  return array


def getyx(array, y, x):
  assert y >= 0
  assert x >= 0
  return array[y][x]

def setyx(array, y, x, value):
  assert y >= 0
  assert x >= 0
  array[y][x] = value
  
def plane_str(plane):
  string = '---start---'
  for y in range(len(plane)):
    for x in range(len(plane[y])):
      tile = getyx(plane, y, x).char
      string += tile + tile
    string += '\n'
  string += '----end----'
  return string


#===========================================================
#  Utility classes
#===========================================================
# https://stackoverflow.com/questions/3603502/

import logging as log
class Deny_New_Fields(object):
  """
  If inherited, allows freezing fields so that the program
  crashes if I misspell a field instead of running along with
  unexpected and unreported behavior.
  This is why I don't like Python.
  """
  
  __deny_new_fields = False
  
  def __setattr__(self, key, value):
    if self.__deny_new_fields and key not in dir(self):
      raise TypeError("`%s' cannot be added to %r" % (key, self))
    object.__setattr__(self, key, value)
    
  def _deny_new_fields(self):
    self.__deny_new_fields = True
