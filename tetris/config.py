#
#  Here's the read-only `globals' or rather `consts'.
#  These should be configurable (either with program arguments
#  or with a config file) at the start and then never be modified
#  while the program is running.
#


def get_program_arguments():
    import sys

    return sys.argv


def get_program_arguments_as_dict():
    # TODO implement console program argument > dictionary
    pass


# ===========================================================
#  Global data section
# ===========================================================

ARGS = get_program_arguments()
# global args = get_program_arguments_as_dict()


SKIP_INTRO_CREDITS = True

# Game field width and height
WID = 10
HEI = 20
# Scaling of the render (stretch the game field)
WID_SCALE = 2
# @bug; hei_scale other than 1 breaks render
HEI_SCALE = 1

# Border width, Width of the sidebar
BORDER = 1
SIDEBAR_WID = 7

USE_COLOR = True
HIGHLIGHT_PIECE_POS = False

# Lenght of one game frame (in seconds). The shorter the frametime, the more responsive the app will be.
FRAME_LEN = 0.015
# @warning; Do not tie gameplay time to this
# @idea; somehow test how much the hardware can handle and set it that way??
LINE_DELETION_DELAY_SECS = 0.1

# Indexes of the X and Y coordinates used in this project
#                  THE ~Y~ IS FIRST
# Pretty important to keep in mind, so it's a global
Y_INDEX = 0
X_INDEX = 1
COORDINATE_INDEXES = {"y": Y_INDEX, "Y": Y_INDEX, "x": X_INDEX, "X": X_INDEX}

# Common keys
NO_KEY = -1  # non-blocking getch() result when nothing's been pressed
ESCAPE_KEY = 21
