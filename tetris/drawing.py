#
#  Everything that tetris needs to draw on the screen.
#  This means any change in data should be reflected
#  by drawing somewhere in this file.
#
#  (This file might not be ALL the drawing - some menus
#   might be elsewhere)
#

import logging as log

import curses
import math

from tetris.config import *
from tetris.utils import getyx, add_all_tuples
from tetris.resources import Tiles


# ===========================================================
#  Drawing the game tile data, e.g. pieces
# ===========================================================


def get_tile_color(tile, override_color=None):
    if USE_COLOR:
        if override_color:
            return curses.color_pair(override_color)
        else:
            return curses.color_pair(tile.color)
    else:
        NO_COLOR = 0
        return NO_COLOR


def get_tile_character(tile, transparent=False):
    return "%" if transparent else (" " if USE_COLOR else tile.char)


def translate_coords_to_window(coords):
    return [coords[Y_INDEX] * HEI_SCALE + BORDER, coords[X_INDEX] * WID_SCALE + BORDER]


def tile(coords, char, window):

    win_coords = translate_coords_to_window(coords)
    Y = win_coords[Y_INDEX]
    X = win_coords[X_INDEX]

    for y in range(Y, Y + HEI_SCALE):
        for x in range(X, X + WID_SCALE):
            # allow calling addch on characters
            # outside window coords
            try:
                window.addch(y, x, char)
            except curses.error as err:
                # log.warn("[Warning] Ignoring character")
                pass
            except OverflowError as err:
                log.error("[Error] Unprintable char (%s)" % char)
                raise err


def set_color_attr_of_tile(tile_type, window):
    color = get_tile_color(tile_type)
    window.attron(color)
    return color


def draw_tile(Y, X, tile_type, window, override_color=None, transparent=False):

    # Respect the scale
    Y *= HEI_SCALE
    X *= WID_SCALE
    # Respect the border
    X += BORDER
    Y += BORDER

    # set the color
    color = get_tile_color(tile_type, override_color)
    window.attron(color)
    # in case I want to show only slightly (piece ghosting)
    char = get_tile_character(tile_type, transparent)

    # fill in the tile
    for yyy in range(Y, Y + HEI_SCALE):
        for xxx in range(X, X + WID_SCALE):

            # wrapping the draw in try/catch;
            # curses crashes if drawing outside the window
            try:
                window.addch(yyy, xxx, char)

            except curses.error as err:
                log.warn("[Warning] Ignoring character")


def draw_tiles(coord_array, tile_type, window, override_color=None, transparent=False):

    assert type(coord_array) is list
    assert type(coord_array[0]) is list or tuple

    for coord in coord_array:
        x = coord[X_INDEX]
        y = coord[Y_INDEX]
        draw_tile(y, x, tile_type, window, override_color, transparent)


def draw_piece(piece, window, force_y=None, force_x=None, transparent=False):

    y = piece.y if force_y is None else force_y
    x = piece.x if force_x is None else force_x

    tiles = piece.data[piece.rotation]
    tiles_relative_to_pos = add_all_tuples(tiles, [y, x])
    tile_type = Tiles[piece.type]
    # @incomplete; handle the offsetting when moving piece (logic), not on every draw call - it's hugely inefficient right now

    # Prepare color for batch draw
    set_color_attr_of_tile(tile_type, window)

    # Tiles
    for coord in tiles_relative_to_pos:
        tile(coord, get_tile_character(tile_type, transparent), window)

    # Handle
    handle_tile = Tiles["FLY_HANDLE"] if HIGHLIGHT_PIECE_POS else tile_type
    tile((y, x), get_tile_character(handle_tile, transparent), window)


def highlight_lines(lines, line_width, window, color):
    full_tile = Tiles["FULL"]
    for y in lines:
        for x in range(line_width):
            draw_tile(y, x, full_tile, window, color)


def draw_ghost(piece, window):
    # @not_implemented; use a semi-opaque char for this
    draw_piece(piece, window, transparent=True)


def draw_game_plane(game_data, timer, window):

    # Draw the tiles that are already in place
    for x in range(game_data.width):
        for y in range(game_data.height):
            tile = getyx(game_data.plane, y, x)
            draw_tile(y, x, tile, window)

    # Draw the ghost
    ghost = game_data.flying_ghost
    if ghost is not None:
        log.debug("Ghost@ y{}x{}".format(ghost.y, ghost.x))
        draw_piece(ghost, window, transparent=True)

    # Draw the flying piece
    fly = game_data.flying
    if fly is not None:
        log.debug("Fly@ y{}x{}".format(fly.y, fly.x))
        draw_piece(fly, window)

    # Draw the tile highlights, if any
    lines = game_data.lines_to_delete
    if lines is not None:
        color = Tiles["HIGHLIGHT"].color
        width = game_data.width
        highlight_lines(lines, width, window, color)


# ===========================================================
#  Drawing game info in the sidebar
# ===========================================================


def reset_color_to_default(window):
    window.attron(curses.color_pair(1))


def draw_strings(lines, y_start, x_start, window):
    reset_color_to_default(window)
    y = y_start
    for line in lines:
        window.addstr(y, x_start, line)
        y += 1


def draw_horiz_separator(y, window):
    window_width = window.getmaxyx()[X_INDEX]
    line_width = math.ceil(window_width / 2)
    x = int((window_width - line_width) / 2)

    reset_color_to_default(window)
    window.hline(y, x, "`", line_width)


def draw_sidebar(game_data, timer, window):

    BORDER = 1
    PAD = 1

    # upcoming tile
    bar_wid = window.getmaxyx()[X_INDEX]
    # @incomplete; find a way to get the bounds (maxyx) of a tile so I can position upcoming tiles properly
    x = 2
    y = 2
    draw_piece(game_data.upcoming, window, y, x)

    # text lines
    stats = game_data.stats
    item = stats.entries
    level = stats.level

    lines = [
        "Level: {}".format(level.current),
        "Score: ",
        " {}".format(stats.calculate_score()),
        "Lines: {}".format(item["line_cleared"].counter),
        "Planted:",
        " {}".format(item["piece_planted"].counter),
        "Slammed:",
        " {}".format(item["piece_slammed"].counter),
    ]
    bar_hei = window.getmaxyx()[Y_INDEX]
    text_y = bar_hei - (len(lines) + PAD + BORDER)
    text_x = PAD + BORDER

    animation = "animate_level_gained"
    # Prepare the timer when gaining the level
    if level.gained_this_frame:
        log.debug("Start timer for level gained animation")
        anim_int = timer.add_interval(animation, 0.5)
        anim_int.force_redraws = True
        anim_int.full_reset()

    # Check the timer if it's prepared and triggered
    animation_interval = timer.intervals.get(animation)
    if animation_interval != None:
        log.debug("Animation timer exists")
        if not animation_interval.triggered:
            # Draw the new level animation
            log.debug("Level gained animation started")
            string = "  NEW LEVEL  "
            window_width = window.getmaxyx()[X_INDEX]
            x = int((window_width - len(string)) / 2)
            window.addstr(text_y - 1, x, string)

    draw_strings(lines, text_y, text_x, window)

    # separator from tile listing
    hline_y = text_y - (2 * PAD + BORDER)
    draw_horiz_separator(hline_y, window)
