

## Bugs

    minor flashing when deleting a line :: test

    no-color mode is broken (doesn't even compile)
    

## Features


    Improve the way things are drawn, I'm not happy with it.
    It's way too crowded and passed over, things are done
    more often than they need to... so decouple, let things
    be copypasted instead of this mess of abstraction.
    
    
    Make a drop-lock independent from level
    ...right now sliding the pieces is impossible, they get 
       immediatelly placed down (next fly_descent places it)
       
       -> add a delay separate from fly_descent
       

    Gaining-level-delay :: independent from level speed
    ...let the user read it, have a moment of respite
    
    Piece-drop-delay :: independent from level speed
    ...sometimes a piece is thrown down immediatelly
    ...note; do not let a key press propagate through delay
    
    
    
    T-spins (scoring bonuses etc.)
    
    find a way to make sounds
    make sounds 

    a start screen
    ...with options to change the config
    
    changable keybinds
    
    a separate config files without functions that would include keys


## Tweaks / Refactoring
    
    save an array of ints as `game_data.plane', not pointers to Enums
    
    tweak the keybindings
    
    the game over screen isn't rendered full-screen, dummy
    
    minimise the use of `game_data' in program arguments
    
    use the color attron() only when i actually need it 
    (only when i actually want to draw the whole piece, 
    no need to do it with every tile)
    
    don't reuse `draw_tile()' so aggresively
    
    
    !! write a goddamn README.MD, you loser !!
    










